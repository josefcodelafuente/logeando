package es.jf.ejemplos;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
 
public class Foo {
  static final Logger logger = LogManager.getLogger(Foo.class.getName());
 
  public boolean doIt() {
    logger.traceEntry();
    logger.error("Did it again!");
    return logger.traceExit(false);
  }
}
