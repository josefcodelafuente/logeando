import es.jf.ejemplos.Bar;
import es.jf.ejemplos.Foo;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public final class App {

    private static final Logger logger = LogManager.getLogger(App.class);
 
    public static void main(final String... args) {
 
        logger.trace("Entering application.");
        Bar bar = new Bar();
        if (!bar.doIt()) {
            logger.error("Didn't do it.");
        }

        logger.trace("traceamos");

        Foo foo = new Foo();
        if (!foo.doIt()) {
            logger.error("Didn't do it.");
        }

        logger.trace("Exiting application.");
    }
}
